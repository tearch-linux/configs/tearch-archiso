>## TeArch Archiso
>You can build TeArch ISO with this tool.
>
>
>### How to build ISO?
>Install needed packages with Pacman:
>
>`sudo pacman -S archiso mkinitcpio-archiso git squashfs-tools --needed`
>
>Clone tearch-archiso:
>
>`git clone https://gitlab.com/tearch-linux/tearch-archiso.git`
>
> `cd tearch-archiso`
>
>Build ISO:
>
>`sudo ./mktearchiso -v -n -d xfce` (example for xfce)





## To Do:

- [ ] System maintenance app.
- [ ] Kernel management app.
- [ ] Welcome app.
- [ ] Driver manager app.
- [ ] Startup and shutdown sounds.
- [ ] Sound theme for Xfce edition.
- [ ] Mirrors for TeArch repo.
- [ ] Kernel based bootsplash.
- [ ] Netinstall support.
- [ ] GUI for create ISO.



